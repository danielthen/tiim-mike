<?php
    $leht = $_GET['leht'];
    $keel = $_GET['keel'];

    $est = array(
        "kodu" => "KODU",
        "tulemused" => "TULEMUSED",
        "lugu" => "LUGU",
        "meist" => "MEIST",
        "loo" => "LOO KONTO",
        "profiil" => "PROFIIL",
        "muuda" => "MUUDA",
        "kontakt" => "KONTAKT",
        "kulaline" => "KÜLALISENA",
        "saada_kiri" => "<em>Saada kiri ...</em>",
        "kust_leida" => "<em>Tiim Mike'i liikmeid leiate Tartu Ülikooli J.Liivi 2 õppehoonest.</em>",
        "sinu_nimi" => "Sinu nimi:<br />",
        "sinu_email" => "E-maili aadress:<br />",
        "sinu_sonum" => "Sõnum:<br />",
        "kasutajanimi" => "Kasutajanimi",
        "parool" => "Parool",
        "loo_pealkiri" => "<em>Ühel ilusal ööl ...</em>",
        "loo_l1" => "<em>Pärast suurt rännakut leidis Seene Mikk lõpuks ometi kodutee. Tema vaenlased kurjad orad olid juba seljataga. Aeg oli minna koju Seenlasse, kus valmistada üks suur seenesoust ja elada õnnelikult elu lõpuni.</em>",
        "loo_l2" => "<em>Seenla, paik, mida maailm tunneb kui seente kuningriigina. Seal katab maapinda suur ja roheline seenevaip, puude küljes elavad punased ja kollased seened ning mida kõrgemale, seda sinisemaks seened muutuvad. See on paik, kus ei ole kurja, vaid seened ja tema elanikud.</em>",
        "loo_l3" => "<em>Seene Mikk oli oma venna Seene Mike'iga kokku leppinud, et saavad kokku Seenlas Suure Kärbseseene juures. Läbi hüüfide aga saabus Seene Mike'ile teade, et Mikk oli röövitud öövalvurite poolt.</em>",
        "loo_l4" => "<em>Seene Mike haarab enda taskulambi ja läheb Mikku päästma.</em>",
        "loo_l5" => "<em><strong>Kas ka Sina oled valmis teda aitama?</strong></em>",
        "meist_pealkiri" => "<em>Üks hämmastav meeskond ...</em>",
        "meist_l1" => "<em>Seene Mike'i valmistaja on Tartu Ülikooli 'Veebirakenduste loomine' kursuse <strong> Tiim Mike </strong>. Tiim Mike'i ühendab
        ühine huvi arvutimängude mängimise ja loomise vastu.
        Hetkel kannab ta hoolt selle eest, et anda Seene Mike'ile enda kodu veebis ja võimalus päästa enda vend kurjade öövalvurite käest.</em>",
        "meist_l2" => "<em>Tiim Mike liikmed:</em>",
        "meist_l3" => "<em> Tiim Mike'i muskel</em>",
        "meist_l4" => "<em> Tiim Mike'i aju </em>",
        "meist_l5" => "<em> Tiim Mike'i süda </em>",
        "konto" => "KONTO",
        "reg_pealkiri" => "<em> Aeg tulla külla Seenlasse ... </em>",
        "reg_kasutaja" => "Kasutajanimi:",
        "reg_parool" => "Parool:",
        "reg_parool2" => "Korda parooli:",
        "reg_email" => "Meiliaadress:",
        "reg_file" => "Profiilipilt:",
        "skoor_info" => "Vali sobivad väärtused.",
        "skoor_mng" => "Mängijate arv:",
        "skoor_maailm" => "Maailm:",
        "seenena" => "SEENENA",
        "parim_tulemus" => "Parim tulemus:",
        "kokku_aega" => "Kokku mänguaega:",
        "lemmik_valjak" => "Lemmikväljak:",
        "vlju" => "VÄLJU",
		"link1" => "Tagasi pealehele!",
		"link2"=> "Tagasi <em>konto</em> lehele!",
		"kontoEdu" => "Täname kirja eest! Vastame Teile esimesel võimalusel.",
        "kontoViga"=> "Tundub, et midagi läks valesti. Palun proovige uuesti.",
        "edukas_title" => "Edukas tegevus",
        "index_title" => "Seene Mike",
        "kontakt_title" => "Seene Mike'i kontakt",
        "login_title" => "Seene Mike'i logimine",
        "lugu_title" => "Seene Mike'i lugu",
        "meist_title" => "Seene Mike'i valmistajast",
        "regist_title" => "Seene Mike'i registreerimine",
        "skoor_title" => "Seene Mike'i tulemused",
        "viga_titlle" => "Tekkis viga",
        "mang_title" => "Seene Mike'i mäng",
        "new_parool" => "Uus parool:",
        "new_email" => "Meiliaadress:",
        "kustuta" => "Kustuta kasutaja!",
        "new_file" => "Uus profiilipilt:"
    );

    $eng = array(
        "new_parool" => "New password:",
        "new_email" => "New e-mail address:",
        "kustuta" => "Delete account!",
        "new_file" => "New profile picture:",
        "vlju" => "EXIT",
        "parim_tulemus" => "Best result:",
        "kokku_aega" => "Played time:",
        "lemmik_valjak" => "Favorite map:",
        "seenena" => "AS MUSHROOM",
        "skoor_mng" => "Number of players:",
        "skoor_maailm" => "World:",
        "reg_kasutaja" => "User name:",
        "reg_parool" => "Password:",
        "reg_parool2" => "Repeat password:",
        "reg_email" => "E-mail:",
        "reg_file" => "Profile picture:",
        "reg_pealkiri" => "<em> Time to visit Seenla ... </em>",
        "konto" => "PROFILE",
        "meist_pealkiri" => "<em>An Amazing Team ...</em>",
        "meist_l1" => "<em>The maker of Seene Mike is Team Mike of University of Tartu. Team Mike has always been interested in developing and playing computer games.
        Right now their main mission is to take care of Seene Mike and to give him a chance to save his brothers from the evil nightguards.</em>",
        "meist_l2" => "<em>Members of Team Mike:</em>",
        "meist_l3" => "<em>The Muscle of Team Mike</em>",
        "meist_l4" => "<em>The Brain of Team Mike</em>",
        "meist_l5" => "<em>The Heart of Team Mike</em>",
        "loo_l1" => "<em>After a great journey Seene Mikk finally found a way back to home. His enemies, evil spikes, were behind. It was time to go home to Seenla where to make a big mushroom sauce and to live
        happily ever after.</em>",
        "loo_l2" => "<em>Seenla, a place that world knows as Kingdom of Mushrooms. There the ground is covered by a big green blanket of mushrooms, on the trees there are living red and yellow mushrooms and the higher it gets the bluish mushrooms are. This is the place 
        where is no evil, only mushrooms and its other inhabitants.</em>",
        "loo_l3" => "<em>Seene Mikk agreed with his brother Seene Mike that they are going to meet in Seenla near the Big Amanita. Through hyphae a message came to Seene Mike saying that Mikk was kidnapped by nighguards.</em>",
        "loo_l4" => "<em>Seene Mike grabs his flashlight and goes to save Mikk.</em>",
        "loo_l5" => "<em><strong>Are you too ready to help him?</strong></em>",
        "loo_pealkiri" => "<em>A Beautiful Night ...</em>",
        "kasutajanimi" => "User name",
        "parool" => "Password",
        "kulaline" => "AS GUEST",
        "kodu" => "HOME",
        "tulemused" => "SCOREBOARD",
        "lugu" => "STORY",
        "meist" => "ABOUT US",
        "loo" => "SIGN UP",
        "profiil" => "PROFILE",
        "muuda" => "CHANGE",
        "kontakt" => "CONTACT",
        "saada_kiri" => "<em>Send a letter ...</em>",
        "kust_leida" => "<em>Team Mike is located at University of Tartu, J.Liivi 2.</em>",
        "sinu_nimi" => "Your name:<br />",
        "sinu_email" => "E-mail address:<br />",
        "sinu_sonum" => "Message:<br />",
        "skoor_info" => "Choose suitable values.",
		"link1" => "Back to Main page!",
		"link2" => "Back to <em>contact</em> page!",
		"kontoEdu" => "Thank you for your letter. We will reply as soon as possible.",
        "kontoViga" => "It seems that something went wrong. Please try again.",
        "edukas_title" => "Successful action",
        "index_title" => "Seene Mike",
        "kontakt_title" => "Seene Mike contact",
        "login_title" => "Seene Mike login",
        "lugu_title" => "Seene Mike story",
        "meist_title" => "Seene Mike creators",
        "regist_title" => "Seene Mike registration",
        "skoor_title" => "Seene Mike scores",
        "viga_titlle" => "An error has occured",
        "mang_title" => "Game of Seene Mike"
    );


    if($keel == 'eng')
    {
        $kasutatav = $eng;
    }
    else
    {
        $kasutatav = $est;
    }

    if($leht == "index.html")
    {
        echo "index_title".";".$kasutatav["index_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kulaline".";".$kasutatav["kulaline"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "seenena".";".$kasutatav["seenena"];
    }
    else if($leht == "kontakt.html")
    {
        echo "kontakt_title".";".$kasutatav["kontakt_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "sinu_nimi".";".$kasutatav["sinu_nimi"]."<<eraldaja>>";
        echo "sinu_email".";".$kasutatav["sinu_email"]."<<eraldaja>>";
        echo "sinu_sonum".";".$kasutatav["sinu_sonum"]."<<eraldaja>>";
        echo "saada_kiri".";".$kasutatav["saada_kiri"]."<<eraldaja>>";
        echo "kust_leida".";".$kasutatav["kust_leida"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"];
    }
    else if($leht == "login.html")
    {
        echo "login_title".";".$kasutatav["login_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kasutajanimi".";".$kasutatav["kasutajanimi"]."<<eraldaja>>";
        echo "parool".";".$kasutatav["parool"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"];
    }
    else if($leht == "lugu.html")
    {
        echo "lugu_title".";".$kasutatav["lugu_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "loo_pealkiri".";".$kasutatav["loo_pealkiri"]."<<eraldaja>>";
        echo "loo_l1".";".$kasutatav["loo_l1"]."<<eraldaja>>";
        echo "loo_l2".";".$kasutatav["loo_l2"]."<<eraldaja>>";
        echo "loo_l3".";".$kasutatav["loo_l3"]."<<eraldaja>>";
        echo "loo_l4".";".$kasutatav["loo_l4"]."<<eraldaja>>";
        echo "loo_l5".";".$kasutatav["loo_l5"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"];
    }
    else if($leht == "meist.html")
    {
        echo "meist_title".";".$kasutatav["meist_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "meist_pealkiri".";".$kasutatav["meist_pealkiri"]."<<eraldaja>>";
        echo "meist_l1".";".$kasutatav["meist_l1"]."<<eraldaja>>";
        echo "meist_l2".";".$kasutatav["meist_l2"]."<<eraldaja>>";
        echo "meist_l3".";".$kasutatav["meist_l3"]."<<eraldaja>>";
        echo "meist_l4".";".$kasutatav["meist_l4"]."<<eraldaja>>";
        echo "meist_l5".";".$kasutatav["meist_l5"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"];
    }
    else if($leht == "regist.html")
    {
        echo "regist_title".";".$kasutatav["regist_title"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "reg_pealkiri".";".$kasutatav["reg_pealkiri"]."<<eraldaja>>";
        echo "reg_kasutaja".";".$kasutatav["reg_kasutaja"]."<<eraldaja>>";
        echo "reg_parool".";".$kasutatav["reg_parool"]."<<eraldaja>>";
        echo "reg_parool2".";".$kasutatav["reg_parool2"]."<<eraldaja>>";
        echo "reg_email".";".$kasutatav["reg_email"]."<<eraldaja>>";
        echo "reg_file".";".$kasutatav["reg_file"];
    }
    else if($leht == "skoor.html")
    {
        echo "skoor_title".";".$kasutatav["skoor_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "skoor_info".";".$kasutatav["skoor_info"]."<<eraldaja>>";
        echo "skoor_mng".";".$kasutatav["skoor_mng"]."<<eraldaja>>";
        echo "skoor_maailm".";".$kasutatav["skoor_maailm"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"];
    }
    else if($leht == "profiil.php")
    {
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "reg_kasutaja".";".$kasutatav["reg_kasutaja"]."<<eraldaja>>";
        echo "parim_tulemus".";".$kasutatav["parim_tulemus"]."<<eraldaja>>";
        echo "kokku_aega".";".$kasutatav["kokku_aega"]."<<eraldaja>>";
        echo "lemmik_valjak".";".$kasutatav["lemmik_valjak"]; 
    }
    else if($leht == "mang.html")
    {
        echo "mang_title".";".$kasutatav["mang_title"]."<<eraldaja>>";
        echo "vlju".";".$kasutatav["vlju"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]; 
    }
    else if($leht == "edukas.html")
    {
        echo "edukas_title".";".$kasutatav["edukas_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kontoEdu".";".$kasutatav["kontoEdu"]."<<eraldaja>>";
		echo "link1".";".$kasutatav["link1"]."<<eraldaja>>";
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"];
    }
	else if($leht == "viga.html")
    {
        echo "viga_title".";".$kasutatav["viga_title"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "kontoViga".";".$kasutatav["kontoViga"]."<<eraldaja>>";
		echo "link1".";".$kasutatav["link1"]."<<eraldaja>>";
        echo "link2".";".$kasutatav["link2"]."<<eraldaja>>";
		echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "loo".";".$kasutatav["loo"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"];
    }
    else if($leht == "muuda.php")
    {
        echo "kodu".";".$kasutatav["kodu"]."<<eraldaja>>";
        echo "konto".";".$kasutatav["konto"]."<<eraldaja>>";
        echo "tulemused".";".$kasutatav["tulemused"]."<<eraldaja>>";
        echo "meist".";".$kasutatav["meist"]."<<eraldaja>>";
        echo "profiil".";".$kasutatav["profiil"]."<<eraldaja>>";
        echo "kontakt".";".$kasutatav["kontakt"]."<<eraldaja>>";
        echo "lugu".";".$kasutatav["lugu"]."<<eraldaja>>";
        echo "muuda".";".$kasutatav["muuda"]."<<eraldaja>>";
        echo "new_parool".";".$kasutatav["new_parool"]."<<eraldaja>>";
        echo "new_email".";".$kasutatav["new_email"]."<<eraldaja>>";
        echo "kustuta".";".$kasutatav["kustuta"];
    }

?>