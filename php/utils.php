<?php
function getUserId($dbCon, $username){
	$usersindb = $dbCon->prepare('SELECT id FROM kasutajad WHERE kasutajanimi = ?');
	$usersindb -> bind_param('s',$username);
	//$usersindb = $dbCon->prepare("CALL getUserByName('$username')");
	$usersindb -> execute();
	$usersindb -> bind_result($userid);
	$usersindb -> fetch();
	return $userid;
}
?>