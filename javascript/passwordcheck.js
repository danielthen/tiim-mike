var password = document.getElementById("parool")
  , confirm_password = document.getElementById("parool2");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Paroolid ei katTu");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
function register(){
    document.getElementById("error").innerHTML = "";
    var nimi = document.getElementById("kasutajanimi").value;
    var parool = document.getElementById("parool").value;
    var parool2 = document.getElementById("parool2");
    var email = document.getElementById("email").value;
    var file = document.getElementById("file").files[0]; 
    
    var fileLocation = "";
    var formData = new FormData();
    if(nimi == "" || parool == ""||email == ""){
      document.getElementById("error").innerHTML = "Kõik väljad peavad täidetud olema";
      return;
    }
    if(password.value != confirm_password.value) {
      document.getElementById("error").innerHTML = "Paroolid ei kattu";
      return;
    }
    if(isFileOk(file) == 1){
      fileLocation = "../Profiilipildid/default.png"
    }else if(isFileOk(file) == 2){
      fileLocation = "../Profiilipildid/"+file.name;
      formData.append("file",file,file.name);
    }else{
      document.getElementById("error").innerHTML = isFileOk(file);
      return;
    }
    console.log(isFileOk(file));
    console.log("Current image: "+fileLocation);
    var xmlRequest = new XMLHttpRequest();
    var url = "../php/register.php?"+"kasutajanimi="+nimi+"&parool="+parool+"&email="+email+"&filelocation="+fileLocation;
    xmlRequest.onreadystatechange=function() {
        if (xmlRequest.readyState === 4) {
            if (xmlRequest.status === 200) {
                if(xmlRequest.responseText == "1"){
                  window.location.replace("../profiil.php");
                }else{
                  document.getElementById("error").innerHTML = xmlRequest.responseText;
                }
            }
        }
    };
    xmlRequest.open("POST",url ,true);
    xmlRequest.send(formData);
}
function isFileOk(file){
  if(file == null){
    return 1;
  }
  var fileOk = 1;
  if(String(file.name).split(".")[1] != "png"&&
    String(file.name).split(".")[1] != "jpg"&&
    String(file.name).split(".")[1] != "gif"&&
    String(file.name).split(".")[1] != "jpeg"){
    return "File must be png, jpg, gif or jpeg.";
  }
  if(file.size/1024/1024 > 1){
    return "File is too large ["+file.size/1024/1024+"Mb/1Mb].";
  }
  return 2;
}