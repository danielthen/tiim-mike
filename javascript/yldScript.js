function kontroll(){
	var prof = document.getElementById("profiil");
	var loo = document.getElementById("loo");
	var muu = document.getElementById("muuda");
	
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
			if(this.responseText == 1){
				prof.className = "menu";
				muu.className = "menu";
				loo.className = "peidus";
				}
			else{
				prof.className = "peidus";
				muu.className = "peidus";
				loo.className = "menu";
			}
       }
    };
    xhttp.open("GET", "../php/sessionCheck.php", true);
    xhttp.send();
}
function nupuvajutus(key){
	if(event.key === key){
		login();
	}
}
function login(){
	var kasutajanimi = document.getElementById("kasutajanimi").value;
	var parool = document.getElementById("parool").value;
	console.log("Trying to log in");
	var xhttp = new XMLHttpRequest();
	var url = "../php/login.php?kasutajanimi="+kasutajanimi+"&parool="+parool;
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
			console.log(this.responseText);
			if(this.responseText == "1"){
			window.location.replace("./ManguFailid/mang.html");}
			else if(this.responseText=="2"){
				window.location.replace("../profiil.php");
			}
			else if(this.responseText=="3"){
				window.location.replace("../muuda.php");
			}else{
				kasutajanimi = "";
				parool = "";
				document.getElementById("error").innerText = this.responseText;
			}
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}
function navBar(y) {
	//navBar up and down
    var x = document.getElementById("myNav");
    if (x.className == "navbar") {
        x.className += " responsive";
		y.classList.toggle("change");
    } else {
        x.className = "navbar";
		y.classList.toggle("change");
    }

}
function vahetaKeel(leht,keel)
{
    var xmlRequest2 = new XMLHttpRequest();
    var url = "../php/changeLang.php?lang="+keel;
    xmlRequest2.onreadystatechange=function() {
        if (xmlRequest2.readyState === 4) {
            if (xmlRequest2.status === 200) {
                console.log("Praegu on keel: "+xmlRequest2.responseText);
                console.log("Praegu on keel: "+keel);
                muuda(leht, keel);
            }
        }
    };
    xmlRequest2.open("GET",url ,true);
    xmlRequest2.send();
}

function keel(leht){
    var xmlRequest2 = new XMLHttpRequest();
    var url = "../php/keel.php?";
    xmlRequest2.onreadystatechange=function() {
        if (xmlRequest2.readyState === 4) {
            if (xmlRequest2.status === 200) {
                var keel = xmlRequest2.responseText;
                console.log("Praegu on keel: "+keel);
                muuda(leht, keel);
            }
        }
    };
    xmlRequest2.open("GET",url ,true);
    xmlRequest2.send();
}
function muuda(leht, keel){
    var xmlRequest2 = new XMLHttpRequest();
    var url = "../php/lang.php?"+"leht="+leht+"&keel="+keel;
    xmlRequest2.onreadystatechange=function() {
        if (xmlRequest2.readyState === 4) {
            if (xmlRequest2.status === 200) {
                var t6lked = xmlRequest2.responseText;// id;t6lge,id;t6lge,.....
                //console.log("Saime sellise tagastuse: "+t6lked);
                var mingilist = t6lked.split("<<eraldaja>>");
                
                for(var a in mingilist){
                    //console.log("rida: "+mingilist[a].split(":"));
                    var element = mingilist[a].split(";");
                    
                    var id = element[0];
                    var t6lge = element[1];
                    var muutuja = document.getElementById(id);
                    //console.log(id +"->"+t6lge);
                    console.log(t6lked);
                    muutuja.innerHTML = t6lge;
                    
                }
            }
        }
    };
    xmlRequest2.open("GET",url ,true);
    xmlRequest2.send();
}