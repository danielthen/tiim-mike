function myMap() {
    var mapProp= {
        center:new google.maps.LatLng(58.378232, 26.714648),
        zoom:14,
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var markerProp = {
        position: mapProp.center,
        animation: google.maps.Animation.BOUNCE
    }
    var marker=new google.maps.Marker(markerProp);
    marker.setMap(map);
    
    google.maps.event.addListener(marker, 'click', function(){
        map.setZoom(map.getZoom()+1);
        map.setCenter(marker.getPosition());
    })
}