function statusChangeCallback(response) {
    console.log('Proovime sisse logida');
    console.log(response);
    /*
        Võimalikke seisundeid on 3:
            1. 'connected' - kasutaja on sisse logitud ja tal on luba kasutada meie rakendust.
            2. 'non_authorized' - kasutaja on sisse logitud, aga tal pole luba kasutada.
            3. 'unknown' - kasutaja pole kas sisse loginud, või on rakendusest välja loginud.
    */
    if (response.status === 'connected') {
        login();
    } else {
        console.log("Kasutajal pole kas õigusi, või on mingi muu teadmata viga.");
    }
}

// See meetod kutsutakse välja, kui kasutaja on avanenud aknas edukalt sisse loginud.
function LogiKontroll() {
    console.log("Prooviti sisse logida");
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    }, {scope: 'email'});
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '544202862647147',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
    });
};

// Load the SDK asynchronously
(function(d, s, id) 
{
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}
(document, 'script', 'facebook-jssdk')
);

// funktsioon, mis logib kasutaja sisse, ning kontrollib, kas kasutaja on varem mänginud.
function login() {
    
    console.log('Welcome!  Fetching your information..');
    FB.api('/me', function(response) {
        $nimi = response.name;
        $id = response.id;
        $email = response.email;

        console.log("XML created, going into PHP, 2");
        var url = "../php/facebooklogin.php?"+"name="+$nimi+"&email="+$email+"&id="+$id;
        var xmlRequest = new XMLHttpRequest();
        xmlRequest.onreadystatechange=function() {
            if (xmlRequest.readyState === 4) {
                if (xmlRequest.status === 200) {
                           if(xmlRequest.responseText == 1){
                               window.location.replace("./ManguFailid/mang.html");
                           }else{
                               alert("Failure!");
                           }
                           // here you can use the result (cli.responseText)
                } else {
                           // not OK
                           alert('failure!');
                }
            }
        };
        xmlRequest.open("POST",url ,true);
        xmlRequest.send();
    });
}