function addTable(){
    var map = document.getElementById("Map").value;
    var players = document.getElementById("Player").value;

    var xmlRequest = new XMLHttpRequest();
    var url = "../php/MapScores.php?"+"mapid="+map+"&players="+players;
    xmlRequest.onreadystatechange=function() {
        if (xmlRequest.readyState === 4) {
            if (xmlRequest.status === 200) {
                document.getElementById("txtHint").innerHTML = xmlRequest.responseText;
            }
        }
    };
    xmlRequest.open("POST",url ,true);
    xmlRequest.send();
}