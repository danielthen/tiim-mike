<?php
session_start();
$login = 0;
$con = 0;
$id = 0;
if(isset($_SESSION['loggedin'])){
	$login = 1;
	$con = mysqli_connect("localhost","id4999719_ottsaar1","123qweasd","id4999719_seenemike");
	$id = $_SESSION['id'];
	if(mysqli_connect_errno()){
		echo "Failed to connect to MYSQL: " . mysqli_connect_error();
	}
}
else{
	$_SESSION['location'] = $_SERVER['REQUEST_URI'];
	header('Location: ../login.html');
}
?>
<!DOCTYPE html>
<html lang="et">
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href = "css/mainStyle.css">
<link rel="stylesheet" href = "css/Register.css">
<title>Kasutaja sätted</title>
</head>
<!--kui kirjutada teksti, siis maini div sisu sisse.-->
<body onload="kontroll();keel('muuda.php');">
<div class ="sisu"> 
	<div class="flags" id ="div_lang">
		<img class="flag" onclick="vahetaKeel('muuda.php','est')" src="Pildid/et.png" alt="flag_et">
		<img class="flag" onclick="vahetaKeel('muuda.php','eng')" src="Pildid/en.png" alt="flag_en">
	</div>
	<div class="register-menu">
	<form method="post">
    <label for="parool" id="new_parool">Uus parool:</label>
    <input type="password" name="parool" id = "parool">
	<label for="email" id="new_email">Meiliaadress:</label>
    <input type="text" name="email" id="email">
	<!--
	<label for="file" id="new_file">Uus profiilipilt:</label>
    <input type="file" name="file" id="file">
    -->
	<input type="submit" name="psw" value="">
	</form>
	</div>
	<a href="php/delete.php" class="delete" id="kustuta">Kustuta kasutaja!</a>
	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$pass = "";
	$email = "";
	$changed = 0;
	$upload_dir = "../Profiilipildid/";
	$target_file = "";
	if(isset($_FILES["file"])){
		$target_file = $upload_dir . basename($_FILES["file"]["name"]);	
		echo "$target_file";
	}
	if(!empty($_POST["parool"])){
		$pass = $_POST["parool"];
		if($pass != ""){
		$changed=1;
		$stmt = $con->prepare("UPDATE kasutajad SET parool = ? WHERE id = '$id'");
		$stmt->bind_param('s', $pass); // 's' specifies the variable type => 'string'
		$stmt->execute();
		$stmt->close();
		}
		//mysqli_query($con,"UPDATE kasutajad SET parool = '$pass' WHERE id = '$id'");
	}
	if(!empty($_POST["email"])){
		$email = $_POST["email"];
		if($email != ""){
		$changed=1;
		$stmt = $con->prepare("UPDATE kasutajad SET email = ? WHERE id = '$id'");
		$stmt->bind_param('s', $email); // 's' specifies the variable type => 'string'
		$stmt->execute();
		$stmt->close();
		}
		//mysqli_query($con,"UPDATE kasutajad SET email = '$email' WHERE id = '$id'");
	}
	if($changed == 1){
		echo "<h2>Muudatused on sisse viidud</h2>";
	}
		$con->close();
	}
	?>
</div>
<!--Sellest saab alumine menüüriba läbi CSSi.-->
<div class="navbar" id="myNav">
	<a href="index.html" class="menu" id="kodu">KODU</a>
	<a href="skoor.html" class="menu" id="tulemused">TULEMUSED</a>
	<a href="lugu.html" class="menu" id="lugu">LUGU</a>
	<a href="meist.html" class="menu" id="meist">MEIST</a>
	<div class="dropup">
	<a class="menu" id="konto">KONTO</a>
	<div class = "ducontent">
	<a class="active" href="profiil.php" id="profiil">PROFIIL</a>
	<a href="profiil.php" class="menu" id="muuda">MUUDA</a>
	</div>
	</div>
	<a href="kontakt.html" class="menu" id="kontakt">KONTAKT</a>
	<a href="javascript:void(0);" class="icon" onclick="navBar(this)"> <div class="navIcon1"></div>
<div class="navIcon2"></div>
<div class="navIcon3"></div> </a>
</div>
<script src="javascript/yldScript.js"></script>
</body>
</html>