<?php 
session_start();
$login = 0;
$con = 0;
$id = 0;
if(isset($_SESSION['loggedin'])){
	$login = 1;
	$con = mysqli_connect("localhost","id4999719_ottsaar1","123qweasd","id4999719_seenemike");
	$id = $_SESSION['id'];
}
else{
	$_SESSION['location']=$_SERVER['REQUEST_URI'];
	header("Location: ../login.html");
	$login = 0;
	$con = 0;
}
?>
<!DOCTYPE html>
<html lang="et">
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href = "css/mainStyle.css">
<link rel="stylesheet" href = "css/profiil.css">
<title>Profiili leht</title>
</head>
<!--kui kirjutada teksti, siis maini div sisu sisse.-->
<body onload="keel('profiil.php');">
<div class ="sisu"> 
	<?php
		$kasutaja = "";
		$andmed="";
		$valjak ="";
		if($login == 1){
			$kasutaja = mysqli_fetch_assoc(mysqli_query($con,"SELECT kasutajanimi,profiilipilt FROM kasutajad WHERE id = '$id'"));
			$andmed = mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(m2ngitud_aeg) AS aeg, MAX(skoor) AS pskoor FROM skooritabel WHERE kasutaja_id = '$id'"));
			$valjak  = mysqli_fetch_assoc(mysqli_query($con,"SELECT map, COUNT(map) AS lemmik from skooritabel WHERE kasutaja_id='$id' group by map ORDER BY lemmik DESC LIMIT 1"))['lemmik']; 
		}
		else{
			$kasutaja['profiilipilt']=" ühi";
		}
	?>
	<img class="profiilipilt" src="<?php echo $kasutaja['profiilipilt'];?>" alt="profiilipilt">
	<p id = "reg_kasutaja"> Kasutajanimi: </p> 
		<p><?php echo $kasutaja['kasutajanimi'];?><br></p>
	<p id = "parim_tulemus"> Parim tulemus: </p> 
		<p> <?php echo $andmed['pskoor'];?><br> </p>
	<p id = "kokku_aega">Kokku mänguaega: </p> 
		<p> <?php echo $andmed['aeg'];?><br> </p>
	<p id = "lemmik_valjak">Lemmikväljak: </p> 
		<p><?php echo $valjak;?></p>
	<a href="valja.php"><img class = "not" src="Pildid/not.png" alt = "LOGI VÄLJA"></a>
</div>
<!--Sellest saab alumine menüüriba läbi CSSi.-->
<div class="navbar" id="myNav">
	<a href="index.html" class="menu" id="kodu">KODU</a>
	<a href="skoor.html" class="menu" id="tulemused">TULEMUSED</a>
	<a href="lugu.html" class="menu" id="lugu">LUGU</a>
	<a href="meist.html" class="menu" id="meist">MEIST</a>
	<div class="dropup">
	<a class="menu" id="konto">KONTO</a>
	<div class = "ducontent">
	<a class="active" href="profiil.php" id="profiil">PROFIIL</a>
	<a href="muuda.php" class="menu" id="muuda">MUUDA</a>
	</div>
	</div>
	<a href="kontakt.html" class="menu" id="kontakt">KONTAKT</a>
	<a href="javascript:void(0);" class="icon" onclick="navBar(this)"> <div class="navIcon1"></div>
<div class="navIcon2"></div>
<div class="navIcon3"></div> </a>
</div>
<script src="javascript/yldScript.js"></script>
</body>
</html>